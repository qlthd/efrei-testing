const sinon = require('sinon')
var app = require('../st2tst_js.js')
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
var expect = require('chai').expect
var _ = require('lodash');

const jours = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']
const mois = ['01','02','03','04','05','06','07','08','09','10','11','12']
const annees = _.range(1900, 2101).map(String);

describe('setJourDateDeNaissance()', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })
  it ('updates the HTML select jDN with days of month from 01 to 31', function () {
    expect(document.getElementById('jDn').options.length).to.equal(0);
    app.setJourDateDeNaissance();
    expect(document.getElementById('jDn').options.length).to.equal(31);
    for(let i = 0; i< 31 ; i++){
    	expect(document.getElementById('jDn').options[i].value).to.equal(jours[i]);
    	expect(document.getElementById('jDn').options[i].text).to.equal(jours[i]);
    }
    
    
  });
});

describe('setMoisDateDeNaissance()', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })
  it ('updates the HTML select mDN with months from 01 to 12', function () {
    expect(document.getElementById('mDn').options.length).to.equal(0);
    app.setMoisDateDeNaissance();
    expect(document.getElementById('mDn').options.length).to.equal(12);
    for(let i = 0; i< 12 ; i++){
    	expect(document.getElementById('mDn').options[i].value).to.equal(mois[i]);
    	expect(document.getElementById('mDn').options[i].text).to.equal(mois[i]);
    }
    
    
  });
});

describe('setAnneeDateDeNaissance()', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })
  it ('updates the HTML select aDN with years from 1900 to 2100', function () {
    expect(document.getElementById('aDn').options.length).to.equal(0);
    app.setAnneeDateDeNaissance();
    expect(document.getElementById('aDn').options.length).to.equal(201);
    for(let i = 0; i< 201 ; i++){
    	expect(document.getElementById('aDn').options[i].value).to.equal(annees[i]);
    	expect(document.getElementById('aDn').options[i].text).to.equal(annees[i]);
    }
    
    
  });
});



describe('checkBirth()', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })
  var date = new Date();
  var year = date.getFullYear(); // one more year than current one
  var month = date.getMonth()+1 > 10 ? date.getMonth()+1 : "0"+(date.getMonth()+1);
  var day = date.getDate() > 10 ? date.getDate() : "0"+date.getDate();
  


  it('checks that being born after actual date displays error date', function () {
    year = year + 1; // one more year than current one

    app.setAnneeDateDeNaissance();
    app.setMoisDateDeNaissance();
    app.setJourDateDeNaissance();
    document.getElementById("aDn").value = year ;
    document.getElementById("mDn").value = month;
    document.getElementById("jDn").value = day;
  
    app.checkBirth();
    expect(document.getElementById("errorDateNaissance").style.display).to.equal("block");
    expect(document.getElementById("dateNaissanceMineur").style.display).to.equal("none");
  });

  it('checks that being born less than 18 years before actual date displays minority message', function () {
    year = year - 17; // 17 years less than current one

    app.setAnneeDateDeNaissance();
    app.setMoisDateDeNaissance();
    app.setJourDateDeNaissance();
    document.getElementById("aDn").value = year ;
    document.getElementById("mDn").value = month;
    document.getElementById("jDn").value = day;
  
    app.checkBirth();
    expect(document.getElementById("errorDateNaissance").style.display).to.equal("none");
    expect(document.getElementById("dateNaissanceMineur").style.display).to.equal("block");
  });

  it('checks that being born more than 18 years before actual date displays minority message', function () {
    year = year - 19; // 19 years less than current one

    app.setAnneeDateDeNaissance();
    app.setMoisDateDeNaissance();
    app.setJourDateDeNaissance();
    document.getElementById("aDn").value = year;
    document.getElementById("mDn").value = month;
    document.getElementById("jDn").value = day;
  
    app.checkBirth();
    expect(document.getElementById("errorDateNaissance").style.display).to.equal("none");
    expect(document.getElementById("dateNaissanceMineur").style.display).to.equal("none");
  });


  
  
});


describe('checkMineur()', function () {

  it('checks that being born on 1997-01-01 makes you not minor on 2020-01-01 (23yo)', function () {
    expect(app.checkMineur(new Date('2020-01-01'),new Date('1997-01-01'))).to.equal(false);
  });
  it('checks that being born on 2000-01-01 makes you not minor on 2018-01-01 (18yo)', function () {
    expect(app.checkMineur(new Date('2018-01-01'),new Date('2000-01-01'))).to.equal(false);
  });
  it('checks that being born on 2000-01-01 makes you minor on 2017-12-31 (17yo)', function () {
    expect(app.checkMineur(new Date('2017-12-31'),new Date('2000-01-01'))).to.equal(true);
  });
  
});


describe('checkNumTelephone(e)', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })

  it('checks that no error is displayed when portable phone number is correct', function () {
    document.getElementById('telephonePortable').value="0601020304";
    app.checkNumTelephone(document.getElementById('telephonePortable'));
    expect(document.getElementById("errorNumPortable").style.display).to.equal("none");
  });

  it('checks that error is displayed when portable phone number is not correct', function () {
    document.getElementById('telephonePortable').value="060102030";
    app.checkNumTelephone(document.getElementById('telephonePortable'));
    expect(document.getElementById("errorNumPortable").style.display).to.equal("block");

    document.getElementById('telephonePortable').value="06 01 02 03 04";
    app.checkNumTelephone(document.getElementById('telephonePortable'));
    expect(document.getElementById("errorNumPortable").style.display).to.equal("block");
  });

  it('checks that no error is displayed when domicile phone number is correct', function () {
    document.getElementById('telephoneDomicile').value="0401020304";
    app.checkNumTelephone(document.getElementById('telephoneDomicile'));
    expect(document.getElementById("errorNumDomicile").style.display).to.equal("none");
  });

  it('checks that error is displayed when domicile phone number is not correct', function () {
    document.getElementById('telephoneDomicile').value="040102030";
    app.checkNumTelephone(document.getElementById('telephoneDomicile'));
    expect(document.getElementById("errorNumDomicile").style.display).to.equal("block");

    document.getElementById('telephoneDomicile').value="04 01 02 03 04";
    app.checkNumTelephone(document.getElementById('telephoneDomicile'));
    expect(document.getElementById("errorNumDomicile").style.display).to.equal("block");
  });
  
  
});


describe('checkCodePostal()', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })

  it('checks that no error is displayed when postal code is correct', function () {
    document.getElementById('codePostal').value="75000";
    app.checkCodePostal();
    expect(document.getElementById("ville").value).to.equal("");
    expect(document.getElementById("errorCodePostal").style.display).to.equal("none");
  });

  it('checks that no error is displayed when postal code is 21000 but city name is displayed', function () {
    document.getElementById('codePostal').value="21000";
    app.checkCodePostal();
    expect(document.getElementById("ville").value).to.equal("Dijon");
    expect(document.getElementById("errorCodePostal").style.display).to.equal("none");
  });

  it('checks that error is displayed when postal code is not correct', function () {
    document.getElementById('codePostal').value="1";
    app.checkCodePostal();
    expect(document.getElementById("ville").value).to.equal("");
    expect(document.getElementById("errorCodePostal").style.display).to.equal("block");
  });
  
  
});


describe('checkCityDijon()', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })

  it('checks that city input background gets colored if city input value is not dijon when postal code is 21000', function () {
    document.getElementById("codePostal").value="21000";
    document.getElementById("ville").value="Paris";
    app.checkCityDijon();

    var e = document.createElement("input");
    e.style.background = "#FFD674";
    expect(document.getElementById("ville").style.background).to.equal(e.style.background);
  });

 it('checks that city input background is white when city input value is dijon and postal code is 21000', function () {
    document.getElementById("codePostal").value="21000";
    document.getElementById("ville").value="Dijon";
    app.checkCityDijon();
    expect(document.getElementById("ville").style.background).to.equal("white");
  });


  
});

describe('checkEmail()', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })

  it('checks that no error is displayed when email is correct', function () {
    document.getElementById("email").value="john.doe@mail.fr";
    app.checkEmail();
    expect(document.getElementById("errorEmail").style.display).to.equal("none");

    document.getElementById("email").value="john.doe@mail.com";
    app.checkEmail();
    expect(document.getElementById("errorEmail").style.display).to.equal("none");
  });

 it('checks that error is displayed when email is incorrect', function () {
    document.getElementById("email").value="john.doe@mail.net";
    app.checkEmail();
    expect(document.getElementById("errorEmail").style.display).to.equal("block");

    document.getElementById("email").value="john.doe@mail";
    app.checkEmail();
    expect(document.getElementById("errorEmail").style.display).to.equal("block");
  });


  
});


describe('All check methods', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })

  it('etudiantChecked() : checks that new checkboxes are shown (div) when student is checked', function () {
    app.etudiantChecked();
    expect(document.getElementById("div_qualification_etudiant").style.display).to.equal("block");
    expect(document.getElementById("listePatron").style.display).to.equal("none");
  });

 it('cadreChecked() : checks that nothing new is shown when cadre is checked', function () {
    app.cadreChecked();
    expect(document.getElementById("div_qualification_etudiant").style.display).to.equal("none");
    expect(document.getElementById("listePatron").style.display).to.equal("none");
  });

 it('fonctionnaireChecked() : checks that nothing new is shown when fonctionnaire is checked', function () {
    app.fonctionnaireChecked();
    expect(document.getElementById("div_qualification_etudiant").style.display).to.equal("none");
    expect(document.getElementById("listePatron").style.display).to.equal("none");
  });

 it('patronChecked() : checks that a scrolling list is shown when patron is checked', function () {
    app.patronChecked();
    expect(document.getElementById("div_qualification_etudiant").style.display).to.equal("none");
    expect(document.getElementById("listePatron").style.display).to.equal("block");
  });
  
});



describe('Validate form for Cadre', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })

  it('validateForm() : checks correct page is generated when cadre is selected', function () {
  	app.setAnneeDateDeNaissance();
    app.setMoisDateDeNaissance();
    app.setJourDateDeNaissance();

    document.getElementById("nom").value = "John Doe";		
  	document.getElementById("jDn").value = 01;
  	document.getElementById("mDn").value = 12;
  	document.getElementById("aDn").value = 1975;

	document.getElementById("telephonePortable").value = "0601020304"
	document.getElementById("telephoneDomicile").value = "0401020304";
	document.getElementById("rue").value = "01 rue du soleil";
	document.getElementById("ville").value = "Dark City";
	document.getElementById("codePostal").value = "10000";
	document.getElementById("email").value = "john.doe@mail.fr";

	//Cadre 
  	expect(document.getElementsByName("profession")[0].value).to.equal("Cadre");
	
  	document.getElementsByName("profession")[0].checked = true;

	app.validateForm();
	
	var stringResume = "<head></head><body><h1>Résumé de vos informations</h1>"+ "<p>M/Mme " + "John Doe" + " est né(e) le " + "01/12/1975" + ". "+"M/Mme " + "John Doe"+ " est " + "cadre" + " . "+"</p>";
	stringResume +="Ses coordonées sont les suivantes : "+ "<ul>"+"<li>"+"<p>Téléphone portable : " + "0601020304"+ "</p>"+"</li>"+"<li>"+"<p>Téléphone fixe : " + "0401020304" + "</p>"+ "</li>";
	stringResume +="<li>"+ "<p>Adresse : " + "01 rue du soleil" + " " + "Dark City" + " " + "10000" + "</p>"+ "</li>"	+ "<li>"+ "<p>Adresse email : " + "john.doe@mail.fr" + "</p>"+ "</li>"+ "</ul></body>";
	expect(document.documentElement.innerHTML).to.equal(stringResume);
  });



  
});

describe('Validate form for Fonctionnaire', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })

  it('validateForm() : checks correct page is generated when fonctionnaire is selected', function () {
	app.setAnneeDateDeNaissance();
	app.setMoisDateDeNaissance();
	app.setJourDateDeNaissance();

	document.getElementById("nom").value = "John Doe";    
	document.getElementById("jDn").value = 01;
	document.getElementById("mDn").value = 12;
	document.getElementById("aDn").value = 1975;

	document.getElementById("telephonePortable").value = "0601020304"
	document.getElementById("telephoneDomicile").value = "0401020304";
	document.getElementById("rue").value = "01 rue du soleil";
	document.getElementById("ville").value = "Dark City";
	document.getElementById("codePostal").value = "10000";
	document.getElementById("email").value = "john.doe@mail.fr";

	//Fonctionnaire 
	expect(document.getElementsByName("profession")[2].value).to.equal("Fonctionnaire");

	document.getElementsByName("profession")[2].checked = true;

	app.validateForm();

	var stringResume = "<head></head><body><h1>Résumé de vos informations</h1>"+ "<p>M/Mme " + "John Doe" + " est né(e) le " + "01/12/1975" + ". "+"M/Mme " + "John Doe"+ " est " + "fonctionnaire" + " . "+"</p>";
	stringResume +="Ses coordonées sont les suivantes : "+ "<ul>"+"<li>"+"<p>Téléphone portable : " + "0601020304"+ "</p>"+"</li>"+"<li>"+"<p>Téléphone fixe : " + "0401020304" + "</p>"+ "</li>";
	stringResume +="<li>"+ "<p>Adresse : " + "01 rue du soleil" + " " + "Dark City" + " " + "10000" + "</p>"+ "</li>" + "<li>"+ "<p>Adresse email : " + "john.doe@mail.fr" + "</p>"+ "</li>"+ "</ul></body>";
	expect(document.documentElement.innerHTML).to.equal(stringResume);
  });



  
});





describe('Validate form for Etudiant', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })

  it('validateForm() : checks correct page is generated when étudiant(e) is selected', function () {
  	app.setAnneeDateDeNaissance();
    app.setMoisDateDeNaissance();
    app.setJourDateDeNaissance();

    document.getElementById("nom").value = "John Doe";    
    document.getElementById("jDn").value = 10;
    document.getElementById("mDn").value = 10;
    document.getElementById("aDn").value = 1975;

	document.getElementById("telephonePortable").value = "0601020304"
	document.getElementById("telephoneDomicile").value = "0401020304";
	document.getElementById("rue").value = "01 rue du soleil";
	document.getElementById("ville").value = "Dark City";
	document.getElementById("codePostal").value = "10000";
	document.getElementById("email").value = "john.doe@mail.fr";

	//Etudiant 
	expect(document.getElementsByName("profession")[1].value).to.equal("Etudiant");

	document.getElementsByName("profession")[1].checked = true;

	// Intelligent et motivé

	expect(document.getElementsByName("qualification_etudiant")[0].value).to.equal("Intelligent");
	expect(document.getElementsByName("qualification_etudiant")[1].value).to.equal("Motivé");

	document.getElementsByName("qualification_etudiant")[0].checked = true;
	document.getElementsByName("qualification_etudiant")[1].checked = true;




	app.validateForm();

	var stringResume = "<head></head><body><h1>Résumé de vos informations</h1>"+ "<p>M/Mme " + "John Doe" + " est né(e) le " + "10/10/1975" + ". "+"M/Mme " + "John Doe"+ " est " + "étudiant(e)";
	stringResume += " et " + "intelligent" + "(e) et " + "motivé" + "(e) ";
	stringResume += ". "+"</p>";

	stringResume +="Ses coordonées sont les suivantes : "+ "<ul>"+"<li>"+"<p>Téléphone portable : " + "0601020304"+ "</p>"+"</li>"+"<li>"+"<p>Téléphone fixe : " + "0401020304" + "</p>"+ "</li>";
	stringResume +="<li>"+ "<p>Adresse : " + "01 rue du soleil" + " " + "Dark City" + " " + "10000" + "</p>"+ "</li>" + "<li>"+ "<p>Adresse email : " + "john.doe@mail.fr" + "</p>"+ "</li>"+ "</ul></body>";
	expect(document.documentElement.innerHTML).to.equal(stringResume);
  });



  
});

describe('Validate form for Patron', function () {
  before(function() {
    return JSDOM.fromFile('ST2TST_Sample_App.html')
      .then((dom) => {
        global.window = dom.window;
        global.document = window.document;
      });
  })

  it('validateForm() : checks correct page is generated when patron is selected', function () {
    app.setAnneeDateDeNaissance();
    app.setMoisDateDeNaissance();
    app.setJourDateDeNaissance();


    document.getElementById("nom").value = "John Doe";    
    document.getElementById("jDn").value = 10;
    document.getElementById("mDn").value = 10;
    document.getElementById("aDn").value = 1975;

	document.getElementById("telephonePortable").value = "0601020304"
	document.getElementById("telephoneDomicile").value = "0401020304";
	document.getElementById("rue").value = "01 rue du soleil";
	document.getElementById("ville").value = "Dark City";
	document.getElementById("codePostal").value = "10000";
  	document.getElementById("email").value = "john.doe@mail.fr";

  	//Patron
  	expect(document.getElementsByName("profession")[3].value).to.equal("Patron");
  
	document.getElementsByName("profession")[3].checked = true;

	//11..20
	expect(document.getElementById("listePatron")[1].value).to.equal("11..20");

	document.getElementById("listePatron")[1].selected = true;

	app.validateForm();

	var stringResume = "<head></head><body><h1>Résumé de vos informations</h1>"+ "<p>M/Mme " + "John Doe" + " est né(e) le " + "10/10/1975" + ". "+"M/Mme " + "John Doe"+ " est " + "patron(ne)";
	stringResume += " et la valeur de la liste est : "+ "11..20"+". "+"</p>";
	stringResume +="Ses coordonées sont les suivantes : "+ "<ul>"+"<li>"+"<p>Téléphone portable : " + "0601020304"+ "</p>"+"</li>"+"<li>"+"<p>Téléphone fixe : " + "0401020304" + "</p>"+ "</li>";
	stringResume +="<li>"+ "<p>Adresse : " + "01 rue du soleil" + " " + "Dark City" + " " + "10000" + "</p>"+ "</li>" + "<li>"+ "<p>Adresse email : " + "john.doe@mail.fr" + "</p>"+ "</li>"+ "</ul></body>";
	expect(document.documentElement.innerHTML).to.equal(stringResume);
  });



  
});




  





